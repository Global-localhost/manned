#!/bin/sh

. ./common.sh

MIRROR=http://mirror.ams1.nl.leaseweb.net/archlinux/

case "$1" in
    current)
        index arch --sys arch --mirror $MIRROR --repo core
        index arch --sys arch --mirror $MIRROR --repo extra
        index arch --sys arch --mirror $MIRROR --repo community
        ;;
esac
